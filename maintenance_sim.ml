open Statz

let rng_state = Random.State.make [| 0x1337; 0x533D |]

module Ctmc(State : Intf.Ordered) = struct
  type fat_state = {
      state : State.t ;
      holding : float ;
      jump : State.t Stats.Float.fin_prb ;
    }

  type chain = State.t -> State.t Stats.Float.fin_den
  type fat_chain = fat_state -> fat_state Stats.gen

  let make_fat_state : State.t -> chain -> fat_state =
    fun state chain ->
    let jumps = chain state in
    let jump = Stats.Float.normalize jumps in
    let rate = Stats.Float.total_mass jumps in
    let holding =
      (Stats.Float.exponential ~rate rng_state) in
    { state ; holding; jump }

  let make_fat : chain -> fat_chain =
    fun chain { jump; _ } ->
    (fun rng_state ->
        let state = Stats.Float.sample_prb jump rng_state in
        make_fat_state state chain
      )

  let sample_trajectory ~chain ~init ~until =
    let fat = make_fat chain in
    let rec loop state now acc =
      if now >= until then
        List.rev acc
      else
        let next = fat state rng_state in
        loop next (now +. next.holding) (next :: acc)
    in
    loop (make_fat_state init chain) 0.0 []
end

module Int_ctmc = Ctmc(Int)

let recompute_times : (int * float) list -> (float * float) list =
  fun traj ->
  let _, points =
    List.fold_left (fun (now, acc) (n, spent_in_n) ->
        let now' = now +. spent_in_n in
        let n = float_of_int n in
        (now', (now', n) :: (now, n) :: acc)
      ) (0.0, []) traj in
  List.rev points

(* let init = 1
 *
 * let ehrenfest ~max ~theta =
 *   assert (max > 0) ;
 *   assert (theta > 0.0) ;
 *   let n_a (x : int) = x in
 *   let n_b (x : int) = max - (n_a x) in
 *   fun (x : int) ->
 *   let n_a = n_a x in
 *   let n_b = n_b x in
 *   Stats.density (module Int)
 *     [ (n_a - 1, theta *. float_of_int n_a) ;
 *       (n_a + 1, theta *. float_of_int n_b) ]
 *
 * let chain = ehrenfest ~max:100 ~theta:0.1
 *
 * let traj = Int_ctmc.sample_trajectory ~chain ~init ~until:100.
 *            |> List.map (fun { Int_ctmc.state; holding ; _ } -> (state, holding))
 *
 * let traj' = recompute_times traj
 *
 * let () =
 *   List.iter (fun (time, value) ->
 *       Format.eprintf "at time %f = %f@." time value
 *     ) traj' *)

(* module Gp = Gnuplot *)

(* let () =
 *   let gp = Gp.create () in
 *   Gp.set gp ~title:"Ehrenfest trajectory" ~output:(Gp.Output.create (`X11));
 *   Gp.plot gp (Gp.Series.lines_xy traj');
 *   Gp.close gp *)

(* Peer table *)
module Peer_table =
  struct
    type peer_kind = Original | Fresh
    type t = peer_kind list

    let compare = Stdlib.compare

    let insert peer (state : t) =
      peer :: state

    let delete alpha (state : t) =
      let coin = Stats.Float.coin ~bias:alpha in
      let rec loop state =
        match state with
        | [] -> []
        | [_] -> []
        | x :: tl ->
           if Stats.Float.sample_prb coin rng_state then
             tl
           else
             x :: (loop tl)
      in
      loop state

  end

module Peer_table_vec = Sparse.Sparse_vec.Make(Reals.Float)(Map.Make(Peer_table))

module Peer_ctmc = Ctmc(Peer_table)


let simulate ~max ~theta ~alpha =
  assert (max > 0) ;
  assert (theta > 0.0) ;
  let n_a (x : Peer_table.t) = List.length x in
  let n_b (x : Peer_table.t) = max - (n_a x) in
  fun (x : Peer_table.t) ->
  assert (List.length x <= max) ;
  let n_a = n_a x in
  let n_b = n_b x in
  Stats.Float.density (module Peer_table_vec)
    [ (Peer_table.delete alpha x, theta *. float_of_int n_a) ;
      (Peer_table.insert Peer_table.Fresh x, theta *. float_of_int n_b) ]

let sample_traj ~theta ~alpha ~until =
  let chain = simulate ~max:100 ~theta ~alpha in
  let init = List.init 50 (fun _ -> Peer_table.Original) in
  let count_orig table =
    List.fold_left (fun acc peer ->
        match peer with
        | Peer_table.Original -> acc + 1
        | _ -> acc) 0 table in
  let traj =
    Peer_ctmc.sample_trajectory ~chain ~init ~until
    |> List.map (fun { Peer_ctmc.state; holding ; _ } ->
           (count_orig state, holding)) in
  recompute_times traj

(* module Gp = Gnuplot
 *
 * let () =
 *   Random.self_init ()
 *
 * let () =
 *   let gp = Gp.create () in
 *   Gp.set gp ~title:"Peer table trajectory" ~output:(Gp.Output.create (`Png "peer_survival"));
 *   Gp.plot_many gp [
 *       Gp.Series.lines_xy (sample_traj ~theta:0.1 ~alpha:0.2 ~until:2000.) ;
 *       Gp.Series.lines_xy (sample_traj ~theta:0.1 ~alpha:0.2 ~until:2000.) ;
 *       Gp.Series.lines_xy (sample_traj ~theta:0.1 ~alpha:0.2 ~until:2000.) ;
 *       Gp.Series.lines_xy (sample_traj ~theta:0.1 ~alpha:0.2 ~until:2000.) ;
 *       Gp.Series.lines_xy (sample_traj ~theta:0.1 ~alpha:0.2 ~until:2000.)
 *     ];
 *   Gp.close gp *)
