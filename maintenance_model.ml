open Statz

type state = { m : int; n : int }

module State_space : Intf.Std with type t = state = struct
  type t = state

  let compare x1 x2 =
    let c = Int.compare x1.m x2.m in
    if c = 0 then Int.compare x1.n x2.n else c

  let pp fmtr { m; n } = Format.fprintf fmtr "{ m = %d; n = %d }" m n

  let hash = Hashtbl.hash

  let equal x y = compare x y = 0
end

let state_encoding =
  let open Data_encoding in
  conv (fun { m; n } -> (m, n)) (fun (m, n) -> { m; n }) (tup2 int31 int31)

let measure_encoding : (state * float) list Data_encoding.t =
  let open Data_encoding in
  list (tup2 state_encoding float)

module Pack_float = struct
  module R = Reals.Float
  module K = Stats.Float
end

module Pack_rationals = struct
  module R = Reals.Rational
  module K = Stats.Rational
end

module Pack = Pack_rationals

let make_progress_printer total message =
  let counter = ref 1 in
  fun () ->
    Printf.eprintf "\r%s %d/%d%!" message !counter total ;
    incr counter

let save ~alpha ~total ~horizon =
  let file =
    Format.asprintf
      "alpha=%.2f_total=%d_horizon=%d.bin"
      (Pack.R.to_float alpha)
      total
      horizon
  in
  let ch =
    Stdlib.open_out_gen [Open_wronly; Open_creat; Open_trunc] 0o666 file
  in
  let write data =
    match Data_encoding.Binary.to_bytes measure_encoding data with
    | Error err ->
        Format.eprintf
          "Encoding error: %a@."
          Data_encoding.Binary.pp_write_error
          err ;
        exit 1
    | Ok bytes -> Stdlib.output_bytes ch bytes
  in
  let close () = Stdlib.close_out ch in
  (file, write, close)

let read_file filename =
  let in_ch = open_in_bin filename in
  let buff = Buffer.create 1024 in
  try
    while true do
      Buffer.add_channel buff in_ch 1024
    done ;
    assert false
  with End_of_file -> Buffer.to_bytes buff

let load solution =
  let bytes = read_file solution in
  let acc = ref [] in
  let rec loop ofs rem =
    match Data_encoding.Binary.read measure_encoding bytes ofs rem with
    | Error Not_enough_data -> ()
    | Error err ->
        Format.eprintf
          "Decoding error: %a@."
          Data_encoding.Binary.pp_read_error
          err ;
        exit 1
    | Ok (new_ofs, measure) ->
        acc := Array.of_list measure :: !acc ;
        let rem = Bytes.length bytes - new_ofs in
        loop new_ofs rem
  in
  loop 0 (Bytes.length bytes) ;
  List.rev !acc |> Array.of_list

module type Parameters = sig
  val alpha : Pack.R.t

  val total : int

  val horizon : int

  val legacy : bool
end

module Solve (P : Parameters) = struct
  let () =
    Format.eprintf
      "Solving for alpha = %f, total = %d, horizon = %d@."
      (Pack.R.to_float P.alpha)
      P.total
      P.horizon

  module R = Pack.R
  module State_vec =
    Sparse.Sparse_vec.Make (Pack_float.R) (Map.Make (State_space))

  let alpha_compl = R.(one - P.alpha)

  let alpha_compl_pow = Array.init (P.total * 2) (R.pow alpha_compl)

  let () = Format.eprintf "Computed iterated powers for 1-alpha@."

  let compute_k_a ~m ~n =
    match (m, n) with
    | (0, 0) -> R.(neg one)
    | (0, _n) -> R.one
    | (_, 1) -> P.alpha
    | _ ->
        let acc = ref R.zero in
        for i = 0 to n - 1 do
          acc := R.(!acc + (alpha_compl_pow.(i) * P.alpha))
        done ;
        !acc

  let compute_k_p ~m ~n =
    match (m, n) with
    | (0, 0) -> R.(neg one)
    | (0, _) -> R.zero
    | _ ->
        let acc = ref alpha_compl_pow.(n + m - 1) in
        for i = 0 to m - 2 do
          let power = alpha_compl_pow.(n + i) in
          acc := R.(!acc + (power * P.alpha))
        done ;
        !acc

  let k_a =
    let mat = Array.make_matrix (P.total + 1) (P.total + 1) Pack.R.zero in
    for i = 0 to P.total do
      for j = 0 to P.total do
        mat.(i).(j) <- compute_k_a ~m:i ~n:j
      done
    done ;
    fun ~m ~n ->
      assert (not (m = 0 && n = 0)) ;
      mat.(m).(n)

  let k_p =
    let mat = Array.make_matrix (P.total + 1) (P.total + 1) Pack.R.zero in
    for i = 0 to P.total do
      for j = 0 to P.total do
        mat.(i).(j) <- compute_k_p ~m:i ~n:j
      done
    done ;
    fun ~m ~n ->
      assert (not (m = 0 && n = 0)) ;
      mat.(m).(n)

  let () = Format.eprintf "Computed k_a and k_p@."

  let rec loop ~start ~stop f acc =
    if start = stop + 1 then acc
    else loop ~start:(start + 1) ~stop f (f start acc)

  let error m n =
    Format.eprintf
      "at m = %d, n = %d, errouneous probabilities (k_a = %f, k_p = %f)@."
      m
      n
      (Pack.R.to_float (k_a ~m ~n))
      (Pack.R.to_float (k_p ~m ~n)) ;
    assert false

  let kernel =
    Pack_float.K.kernel
      ~h:(module State_space)
      (module State_vec)
      (fun { m; n } ->
        assert (m + n <= P.total) ;
        assert (m >= 0 && n >= 0) ;
        if m = 0 && n = 0 then [({ m; n = 1 }, Pack_float.R.one)]
        else
          let open R in
          let delete_prob =
            let total = Int.add m n in
            R.(div (of_int total) (of_int P.total))
          in
          let insert_prob = one - delete_prob in
          let k_a = k_a ~m ~n in
          let k_p = k_p ~m ~n in
          let delete_adv_prob = delete_prob * k_a in
          let delete_orig_prob = delete_prob * k_p in
          if Int.equal n P.total then (
            assert (insert_prob = zero) ;
            [ ({ m; n = Int.pred n }, Pack.R.to_float delete_adv_prob);
              ({ m = Int.pred m; n }, Pack.R.to_float delete_orig_prob) ] )
          else
            [ ({ m; n = Int.succ n }, Pack.R.to_float insert_prob);
              ({ m; n = Int.pred n }, Pack.R.to_float delete_adv_prob);
              ({ m = Int.pred m; n }, Pack.R.to_float delete_orig_prob) ])

  let kernel_uniform =
    Pack_float.K.kernel
      ~h:(module State_space)
      (module State_vec)
      (fun { m; n } ->
        assert (m + n <= P.total) ;
        assert (m >= 0 && n >= 0) ;
        if m = 0 && n = 0 then [({ m; n = 1 }, Pack_float.R.one)]
        else
          let open R in
          let total = Int.add m n in
          let delete_prob = R.(div (of_int total) (of_int P.total)) in
          let insert_prob = one - delete_prob in
          let k_a = R.(of_int n / of_int total) in
          let k_p = R.(of_int m / of_int total) in
          let delete_adv_prob = delete_prob * k_a in
          let delete_orig_prob = delete_prob * k_p in
          if Int.equal n P.total then
            [ ({ m; n = Int.pred n }, Pack.R.to_float delete_adv_prob);
              ({ m = Int.pred m; n }, Pack.R.to_float delete_orig_prob) ]
          else
            [ ({ m; n = Int.succ n }, Pack.R.to_float insert_prob);
              ({ m; n = Int.pred n }, Pack.R.to_float delete_adv_prob);
              ({ m = Int.pred m; n }, Pack.R.to_float delete_orig_prob) ])

  let () = Format.eprintf "Computing kernel^n@."

  let measure f =
    let s = Unix.gettimeofday () in
    let res = f () in
    let t = Unix.gettimeofday () in
    (res, t -. s)

  let kernel = if P.legacy then kernel_uniform else kernel

  let probs =
    let (_fname, write, close) =
      save ~alpha:P.alpha ~total:P.total ~horizon:P.horizon
    in
    let exponential = ref kernel in
    for i = 0 to P.horizon - 1 do
      let (kpow, time) =
        measure @@ fun () ->
        let res = !exponential in
        exponential :=
          Pack_float.K.compose ~h:(module State_space) !exponential kernel ;
        res
      in
      Format.eprintf "computing pow = %d (%f)@." (i + 1) time ;
      let (eval, time) =
        measure @@ fun () -> Pack_float.K.eval_kernel { m = 50; n = 0 } kpow
      in
      Format.eprintf "evaluating (%f)@." time ;
      let result =
        List.map (fun (state, p) -> (state, Pack_float.R.to_float p)) eval
      in
      write result
    done ;
    close ()
end

(* let () = Gc.set { (Gc.get ()) with allocation_policy = 2 } *)

module Analyse = struct
  type probabilistic_state = (state * float) array

  type raw_data = probabilistic_state array

  type plot_1d = float list

  module V = Sparse.Sparse_vec.Make (Reals.Float) (Map.Make (Reals.Float))

  let adversarial_concentration state =
    if state.m = 0 then infinity
    else float_of_int state.n /. float_of_int (state.m + state.n)

  let probability_of_adversarial_concentration_above_eps ~eps proba =
    let acc = ref 0.0 in
    for i = 0 to Array.length proba - 1 do
      let (state, p) = proba.(i) in
      let adv_conc = adversarial_concentration state in
      if adv_conc >= eps then acc := !acc +. p
    done ;
    !acc

  let probability_of_successful_attack ~eps data : plot_1d =
    Array.map (probability_of_adversarial_concentration_above_eps ~eps) data
    |> Array.to_list

  type data_point = { attack_success_probability : float; confidence : float }

  let probability_of_successful_attack_after_t ~time ~rate probabilities =
    let measure = Gsl.Randist.poisson_pdf ~mu:(rate *. time) in
    (* Account for the chance of 0 jumps *)
    let total_mass = ref (measure 0) in
    let array =
      Array.mapi
        (fun k proba ->
          let w = measure (k + 1) in
          total_mass := !total_mass +. w ;
          w *. proba)
        probabilities
    in
    let confidence =
      Gsl.Cdf.poisson_P ~k:(Array.length probabilities) ~mu:(rate *. time)
    in
    (* Prevent numerical inaccuracy creep *)
    total_mass := min 1.0 !total_mass ;
    { confidence; attack_success_probability = Array.fold_left ( +. ) 0. array }

  let average_std_population (data : raw_data) : plot_1d * plot_1d * plot_1d =
    let total_population =
      Array.map
        (fun proba ->
          Array.map (fun ({ m; n }, p) -> (float_of_int (m + n), p)) proba)
        data
    in
    let stats =
      Array.map
        (fun prob ->
          let prob = Stats.Float.density (module V) (Array.to_list prob) in
          let mean = Stats.Float.mean prob in
          let var = Stats.Float.variance prob in
          let std = sqrt var in
          (mean, (mean -. std, mean +. std)))
        total_population
      |> Array.to_list
    in
    let (mean, std) = List.split stats in
    let (stdlow, stdhigh) = List.split std in
    (mean, stdlow, stdhigh)

  module Gp = Gnuplot

  let plot_probability_of_successful_attack ~eps file data =
    let plot = probability_of_successful_attack ~eps data in
    Gp.with_ (fun handle ->
        let output = Gp.Output.create `Qt in
        Gp.set
          ~title:
            (Printf.sprintf
               "Prob. of having %.1f%% adversarial peers after n rounds (%s)"
               (eps *. 100.)
               file)
          ~output
          handle ;
        Gp.plot handle @@ Gp.Series.lines plot)

  let plot_average_population data =
    let (avg, stdlow, stdhigh) = average_std_population data in
    Gp.with_ (fun handle ->
        let output = Gp.Output.create `Qt in
        Gp.set handle ~title:"Average population +- std" ~output ;
        Gp.plot_many
          handle
          [Gp.Series.lines avg; Gp.Series.lines stdlow; Gp.Series.lines stdhigh])

  let plot_probability_of_successful_after_t ~horizon ~rate ~eps ~alpha ~data =
    let progress = make_progress_printer horizon "horizon" in
    let probabilities =
      Array.map (probability_of_adversarial_concentration_above_eps ~eps) data
    in
    let points =
      Array.init horizon (fun t ->
          progress () ;
          let time = float_of_int (t + 1) in
          probability_of_successful_attack_after_t ~time ~rate probabilities)
    in
    let points =
      Array.map
        (fun { confidence; attack_success_probability } ->
          let missing_mass = 1.0 -. confidence in
          let lower_bound = attack_success_probability in
          let upper_bound = attack_success_probability +. missing_mass in
          let estimate = attack_success_probability /. confidence in
          assert (lower_bound >= 0.0) ;
          assert (confidence >= 0.0) ;
          assert (confidence <= 1.0) ;
          assert (missing_mass >= 0.0) ;
          (lower_bound, estimate, upper_bound))
        points
    in
    let lower = Array.to_list (Array.map (fun (x, _, _) -> x) points) in
    let esti = Array.to_list (Array.map (fun (_, x, _) -> x) points) in
    let upper = Array.to_list (Array.map (fun (_, _, x) -> x) points) in
    Gp.with_ (fun handle ->
        let fname =
          Printf.sprintf
            "asp_alpha=%.2f_theta=%.2f_threshold=%.1f.png"
            alpha
            rate
            (eps *. 100.)
        in
        let output = Gp.Output.create (`Png_cairo fname) in
        Gp.set
          handle
          ~title:
            (Printf.sprintf
               "Time-dependent (> %.1f%%)-attack success probability (𝛼 = \
                %.2f, θ = %.2f)"
               (eps *. 100.)
               alpha
               rate)
          ~output ;
        Gp.plot_many
          handle
          [ Gp.Series.lines ~color:`Red ~title:"lower bound" lower;
            Gp.Series.lines ~color:`Black ~title:"normalized" esti;
            Gp.Series.lines ~color:`Blue ~title:"upper bound" upper ])
end

let usage fmtr =
  let msg =
    let solve = {|p2p_model solve alpha <float> total <int> horizong <int>|} in
    let attack_success =
      {|analyse <string> attack success with threshold <float>|}
    in
    let attack_success_timed =
      {|analyse <string> attack success with threshold <float> max time <int> with wakeup rate <float>|}
    in
    Format.asprintf
      "Usage:@.%s@.%s@.%s"
      solve
      attack_success
      attack_success_timed
  in
  Format.fprintf fmtr "%s@." msg

let () =
  let args = Array.to_list Sys.argv in
  match args with
  | [] -> assert false
  | [_] -> usage Format.std_formatter
  | _ -> (
      let params = List.tl args in
      match params with
      | ["solve"; legacy; "alpha"; float; "total"; total; "horizon"; horizon] ->
          let legacy =
            if legacy = "legacy" then true
            else if legacy = "new" then false
            else invalid_arg legacy
          in
          let float = float_of_string float in
          let total = int_of_string total in
          let horizon = int_of_string horizon in
          let module _ = Solve (struct
            let alpha = Pack.R.of_float float

            let total = total

            let horizon = horizon

            let legacy = legacy
          end) in
          ()
      | ["analyse"; file; "attack"; "success"; "with"; "threshold"; epsilon] ->
          let eps = float_of_string epsilon in
          let data = load file in
          Analyse.plot_probability_of_successful_attack ~eps file data
      | [ "analyse";
          file;
          "attack";
          "success";
          "with";
          "threshold";
          epsilon;
          "max";
          "time";
          h;
          "with";
          "wakeup";
          "rate";
          r;
          "alpha";
          alpha ] ->
          let eps = float_of_string epsilon in
          let h = int_of_string h in
          let r = float_of_string r in
          let data = load file in
          let alpha = float_of_string alpha in
          Analyse.plot_probability_of_successful_after_t
            ~horizon:h
            ~rate:r
            ~eps
            ~alpha
            ~data
      | ["analyse"; file; "plot"; "average"; "pop"] ->
          let data = load file in
          Analyse.plot_average_population data
      | _ -> usage Format.err_formatter )
