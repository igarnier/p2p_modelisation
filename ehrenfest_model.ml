open Statz

module type Parameters = sig
  val total : int

  val horizon : int
end

type state = { pop : int }

module State_space = struct
  type t = state

  let compare x1 x2 = Int.compare x1.pop x2.pop

  let pp fmtr { pop } = Format.fprintf fmtr "{ pop = %d }" pop
end

let rec pow x n =
  if n = 0 then Sparse.Rational.one
  else if n = 1 then x
  else
    let hpow = pow x (n / 2) in
    if n mod 2 = 0 then Sparse.Rational.(hpow * hpow)
    else Sparse.Rational.(x * hpow * hpow)

let state_encoding =
  let open Data_encoding in
  conv (fun { pop } -> pop) (fun pop -> { pop }) int31

let encoding : (state * float) list array Data_encoding.t =
  let open Data_encoding in
  array (list (tup2 state_encoding float))

let save ~total ~horizon solution =
  match Data_encoding.Binary.to_bytes encoding solution with
  | Error err ->
      Format.eprintf
        "Encoding error: %a@."
        Data_encoding.Binary.pp_write_error
        err ;
      exit 1
  | Ok bytes ->
      let file = Format.asprintf "total=%d_horizon=%d.bin" total horizon in
      let ch = Stdlib.open_out_bin file in
      Stdlib.output_bytes ch bytes ;
      Stdlib.close_out ch

let read_file filename =
  let in_ch = open_in_bin filename in
  let buff = Buffer.create 1024 in
  let rec loop () =
    try
      Buffer.add_channel buff in_ch 1024 ;
      loop ()
    with End_of_file -> Buffer.to_bytes buff
  in
  loop ()

let load solution =
  let bytes = read_file solution in
  match Data_encoding.Binary.of_bytes encoding bytes with
  | Error err ->
      Format.eprintf
        "Decoding error: %a@."
        Data_encoding.Binary.pp_read_error
        err ;
      exit 1
  | Ok solution -> solution

module Solve (P : Parameters) = struct
  let () = Format.eprintf "total = %d, horizon = %d@." P.total P.horizon

  let rec loop ~start ~stop f acc =
    if start = stop + 1 then acc
    else loop ~start:(start + 1) ~stop f (f start acc)

  module State_vec =
    Sparse.Sparse_vec.Make (Reals.Rational) (Map.Make (State_space))

  let kernel =
    let table = Hashtbl.create 101 in
    Stats.Rational.kernel
      (module State_vec)
      (fun { pop } ->
        match Hashtbl.find_opt table { pop } with
        | None ->
            let result =
              if pop = 0 then [({ pop = Int.succ pop }, Sparse.Rational.one)]
              else
                let open Sparse.Rational in
                let delete_prob = pop // P.total in
                let insert_prob = one - delete_prob in
                let () =
                  if not (insert_prob + delete_prob = one) then assert false
                in
                [ ({ pop = Int.succ pop }, insert_prob);
                  ({ pop = Int.pred pop }, delete_prob) ]
            in
            Hashtbl.add table { pop } result ;
            result
        | Some result -> result)

  (* let rec kernel_pow =
   *   let table = Hashtbl.create 101 in
   *   fun n ->
   *   match Hashtbl.find_opt table n with
   *   | None ->
   *      let result =
   *        if n = 1 then kernel
   *        else if n = 2 then
   *          Stats.Rational.compose kernel kernel
   *        else
   *          let half = (n / 2) in
   *          if half mod 2 = 0 then
   *            Stats.Rational.compose (kernel_pow half) (kernel_pow half)
   *          else
   *            Stats.Rational.(compose (kernel_pow half) (kernel_pow (half + 1)))
   *      in
   *      Hashtbl.add table n result;
   *      result
   *   | Some result ->
   *      result *)

  let rec kernel_pow =
    let table = Hashtbl.create 101 in
    fun n ->
      match Hashtbl.find_opt table n with
      | None ->
          let result =
            if n = 1 then kernel
            else if n = 2 then Stats.Rational.compose kernel kernel
            else Stats.Rational.compose (kernel_pow (n - 1)) kernel
          in
          Hashtbl.add table n result ;
          result
      | Some result -> result

  let () = Format.eprintf "Computing kernel^n@."

  let probs =
    Array.init P.horizon (fun i ->
        Format.eprintf "computing pow = %d@." (i + 1) ;
        Stats.Rational.eval_kernel { pop = 50 } (kernel_pow (i + 1)))
    |> Array.map (List.map (fun (state, p) -> (state, Q.to_float p)))

  let () = save ~total:P.total ~horizon:P.horizon probs
end

module Analyse = struct
  type probabilistic_state = (state * float) list

  type raw_data = probabilistic_state array

  type plot_1d = float list

  module V = Sparse.Sparse_vec.Make (Reals.Float) (Map.Make (Reals.Float))

  let average_std_population (data : raw_data) : plot_1d * plot_1d * plot_1d =
    let total_population =
      Array.map
        (fun proba ->
          List.map (fun ({ pop }, p) -> (float_of_int pop, p)) proba)
        data
    in
    let stats =
      Array.map
        (fun prob ->
          let prob = Stats.Float.density (module V) prob in
          let mean = Stats.Float.mean prob in
          let var = Stats.Float.variance prob in
          let std = sqrt var in
          (mean, (mean -. std, mean +. std)))
        total_population
      |> Array.to_list
    in
    let (mean, std) = List.split stats in
    let (stdlow, stdhigh) = List.split std in
    (mean, stdlow, stdhigh)

  module Gp = Gnuplot

  let plot_average_population data =
    let (avg, stdlow, stdhigh) = average_std_population data in
    Gp.with_ (fun handle ->
        let output = Gp.Output.create `Qt in
        Gp.set handle ~title:"Average population +- std" ~output ;
        Gp.plot_many
          handle
          [Gp.Series.lines avg; Gp.Series.lines stdlow; Gp.Series.lines stdhigh])
end

let usage fmtr =
  let msg =
    let solve = {|p2p_model solve alpha <float> total <int> horizong <int>|} in
    let attack_success =
      {|analyse <string> attack success with threshold <float>|}
    in
    Format.asprintf "Usage:@.%s@.%s" solve attack_success
  in
  Format.fprintf fmtr "%s@." msg

let () =
  let args = Array.to_list Sys.argv in
  match args with
  | [] -> assert false
  | [_] -> usage Format.std_formatter
  | _ -> (
      let params = List.tl args in
      match params with
      | ["solve"; "total"; total; "horizon"; horizon] ->
          let total = int_of_string total in
          let horizon = int_of_string horizon in
          let module _ = Solve (struct
            let total = total

            let horizon = horizon
          end) in
          ()
      | ["analyse"; file; "plot"; "average"; "pop"] ->
          let data = load file in
          Analyse.plot_average_population data
      | _ -> usage Format.err_formatter )
