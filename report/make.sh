#!/bin/sh

# generate bibtex stuff
xelatex p2p_modelisation.tex
# bibtex metric_homsets

# compile doc
# pdflatex metric_homsets.tex
# pdflatex metric_homsets.tex

# refresh mupdf
# xdotool search ".*metric_homsets.*pdf.*" windowactivate --sync key r

# xdotool search --name ".*piotr.*" windowactivate
