\documentclass[10pt]{article}  %% ,twocolumn
%\frenchspacing

\usepackage[a4paper,top=4cm,bottom=4cm,outer=3cm,inner=3cm]{geometry}

\usepackage{stmaryrd} %% pour les crochets de transformation [| |]
\usepackage{listings}
\usepackage{url,xspace}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{bbm}
\usepackage{amsthm}
\usepackage{color}
\usepackage{bussproofs}
\usepackage[absolute]{textpos}
\usepackage{helvet}
\usepackage{array}
\usepackage{hyperref}
\usepackage[all]{xy}
\usepackage{yfonts}
\usepackage{algorithmicx}
\usepackage{algpseudocode}
\usepackage{caption}
\usepackage{subcaption}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}
\newtheorem{example}{Example}
\newtheorem{corollary}{Corollary}

\input{macros.tex}

\title{A model for P2P maintenance}

\begin{document}

\maketitle

\tableofcontents

\vspace{5mm}

This documents describes a model for p2p maintenance and analyses its properties.
In particular we study resilience of this maintenance algorithm against eclipse attacks.
Source code can be found here: \url{https://gitlab.com/igarnier/p2p_modelisation/}

\section{Notations}

\newcommand{\peers}{\mcl P}
\newcommand{\neighbours}{\mcl N}
\newcommand{\targetsize}{\mcl S}

Let $\peers$ be a countable set of peers. The algorithm manipulates a set of
``neighbours'' that we will denote $\neighbours \subseteq \peers$.
The algorithm is parameterised with a target cardinality $\mbf S$ for $\neighbours$.
We will work in a continuous-time setting, with the algorithm ``waking up'' at successive
instants denoted $t_1 < t_2 < \ldots$
The set of neighbours at time $t$ will be denoted $\neighbours_t$. A finite \emph{trace}
is a sequence $\sequ{\neighbours_{t_i}}_{i=1}^k$ with strictly increasing $t_i$.
The \emph{age} of a peer $p \in \neighbours_{t_i}$  (for $i \in [1;k]$) relative to such a trace
is $age_i(p) = t_i - \inf \ens{t_j \mid \forall \ell \in [j; i], p \in \neighbours_\ell }$.
The cardinal of a set $X$ will be denoted $card(X)$.

\section{Requirements}

The algorithm must maintain the size of $\neighbours$ to be equal to $\mbf S$, which we
interpret in the asymptotic sense:
\begin{flalign}
  \label{prop:limit_size}
  \lim_{k \rar \infty} \frac{1}{k} \sum_{i = 0}^{k-1} card(\neighbours_{t_i}) = \mbf S
\end{flalign}
The algorithm must be resilient to eclipse attacks: it should be hard for an adversary to
let the set $\neighbours$ converge to a chosen set in a short amount of time (to be made
precise later).

\section{Modelling assumptions}
\label{sec:modelling_assumptions}

We will model the algorithm and the environment in which it executes as a continuous-time
Markov chain (CTMC) -- ie the time intervals between events are exponentially distributed,
and events are distributed according to their relative rates. These rates are parameters of the model:
\begin{itemize}
\item The algorithm has a wakeup rate $w \in \R_{\ge 0}$.
\item Each peer $p$ has a disconnect rate $d_p \in \R_{\ge 0}$. The peer is removed from
  the neighbourhood at the first such event.
\item Each peer $p$ has a piecewise constant ``peer proposal'' function
  $\peers_p : \R_{\ge 0} \rar \wp(\peers)$.
\end{itemize}
The algorithm is prescribed a nonempty initial set of neighbours $\neighbours_{t_0}$.
At each wakeup event at time $t_k$, the algorithm can perform the following,
in this order (wlog).
\begin{itemize}
\item \underline{Gather potential new peers:}
  for each peer $p \in \neighbours_{t_k}$, ask $p$ its proposals $\peers_p(t_k)$
\item \underline{Construct next neighbourhood}: the algorithm selects $\neighbours_{t_{k+1}}$ as
  a subset of $\neighbours_{t_k} \cup (\cup_p P_k(p))$.
\end{itemize}

\section{Eclipse attacks}

Let $\mcl A \subseteq \peers$ a set of peers designated as adversarial. Define the
\emph{adversarial concentration} of a set of peers $X$ as
$adv(X) = \frac{card(X \cap \mcl A)}{card(X)}$. Eclipse attacks are defined in
purely observational terms on a trace.

\begin{definition}[Eclipse attack]
  An \emph{eclipse attack} is a maximal contiguous interval of times
  $\mcl E = \ens{t_k, t_{k+1}, \ldots}$ such that for all
  $t_i \in \mcl E$, $adv(\neighbours_{t_i}) > 0$.
\end{definition}

We define the following additional terms:
\begin{itemize}
\item An eclipse attack \underline{begins} at time $t_k$ if $t_k = min(\mcl E)$.
\item An eclipse attack \underline{ends} at time $t_k$ if $t_k = max(\mcl E)$.
\item An eclipse attack $(1-\eps)$-\underline{succeeds} at time $t_k$ if $adv(\neighbours_{t_k}) \ge 1 - \eps$.
\item The \underline{duration} of an eclipse attack $\mcl E$ is $max(\mcl E) - min(\mcl E)$.
\end{itemize}

%% We cannot expect an algorithm to prevent attacks of all lengths for all $\eps$. We look
%% for a weaker property:

%% \begin{definition}[Attack resilience]
%%   An algorithm is $\eps$-resilient against eclipse attacks if
%%   the minimal length of a successful eclipse attack is proportional to
%%   the sum of ages of peers
%% \end{definition}

\section{A resilient algorithm for maintenance}

We propose an algorithm for maintenance. The algorithm relies
on two building blocks: the first one is a stochastic process that
selects the number of peers to add and remove from the set
of neighbours; the second one is a probabilistic data-structure for
storing peers.

\subsection{Building block 1: A mean-reverting process for peer population control}

Let $\mbf S \in \N$ be a target size. The peer population control process
must be so that Eq. \ref{prop:limit_size} is verified almost surely.

\subsubsection{Basic version: assuming peer that never disconnect}

We initially assume that peers do not spontaneously disconnect (ie
$d_p = 0$ for all $p$).
Our basic population control mechanism is based on the
\emph{Ehrenfest urn model} with $total$ balls and two urns $A$ and $B$.
We recall its definition and properties. Initially, $A$ is empty and $B$
contains $total$ balls. Let us denote $n_A$ the number of balls in $A$
and $n_B$ for $B$. Since the total number of balls is fixed, the
state of the system is entirely described by $n_A$
(ie the state space is $\N$). The discrete-time dynamics of the
system is extremely simple to describe: pick a ball uniformly at
random, and put it in the other urn. It is clear that this
dynamics tends to balance the number of balls in each urn.
A sample of the Ehrenfest process conditioned to start at $0$
and with $total = 100$ is shown in Fig.~\ref{fig:ehrenfest}.

The continuous-time version corresponds to the rate matrix
$Q$:\footnote{This corresponds to \emph{mass action semantics}.}
\DAR{
  Q(n_A, n_A-1) & = & \theta \, n_A \\
  Q(n_A, n_A+1) & = & \theta \, n_B & = & \theta (total - n_A) \\
}
where $\theta > 0$ is a parameter that controls the global speed
of the diffusion process. Observe that if $n_A < n_B$, the rate $Q(n_A, n_A-1)$ is
lower than the rate $Q(n_A,n_A+1)$ and conversely, so the process will tend to
revert to its mean, around $total/2$.

\begin{figure}
  \begin{center}
    \includegraphics[width=10cm]{ehrenfest.png}
  \end{center}
  \caption{A trajectory of the Ehrenfest process}
  \label{fig:ehrenfest}
\end{figure}

It is well known that this model admits the binomial law
with parameters $total$ and $\frac{1}{2}$
as its equilibrium (unique stationary distribution):
\begin{flalign}
  \label{eq:ehrenfest_equilibrium}
  \pi(n) = 2^{-total} {total \choose n}
\end{flalign}
We now consider the CTMC $X_t$ associated to $Q$.

\begin{lemma}
  Let $\sequ{t_k}_k = t_0 < t_1 \ldots$ be a countably infinite sequence of wakeup times.
  Then almost surely,
  \[
  \lim_{k \rar \infty} \frac{1}{k} \sum_{i = 0}^{k-1} X_{t_k} = \mbf S
  \]
\end{lemma}

\begin{proof}
  The ergodic theorem for Markov chains, that we can apply because
  the process admits an equilibrium, allows us to write:
  \[
  \lim_{k \rar \infty} \frac{1}{k} \sum_{i = 0}^{k-1} X_{t_k} = \mbbm E (\pi)
  \]
  The right-hand side is by Eq.~\ref{eq:ehrenfest_equilibrium}
  that of a binomial distribution, ie $\frac{total}{2} = \mbf S$.
\end{proof}

\paragraph{Implementation 1: by simulation.}

An implementation of peer control using this process might work by simulating
the process above and:
\begin{itemize}
\item on events incrementing $n_A$, add a fresh peer to the peer table
\item on events decreasing $n_A$, disconnect from a peer
\end{itemize}
The primitives for adding and disconnecting peers are implemented through the
peer table described in Sec.~\ref{sec:probabilistic_peer_table}.

\paragraph{Implementation 2: averaged dynamics}
Alternatively, one can implement a sampling-free ``mean'' process which might
be good enough for the purposes of the P2P layer: at each wakeup time $t$,
compute the current number of peers $n = card(\neighbours_t)$ and
compute the $\Delta t$ from the previous wakeup time. Then:
\begin{itemize}
\item remove $\theta n \Delta t$ peers
\item add $\theta (total - n) \Delta t$ peers
\end{itemize}
For this to make sense, one clearly needs to pick $\theta$ and
$\Delta t$ such that $\theta \Delta t < 1$\ldots

\subsubsection{Peer population control with spontaneously disconnecting peers (TODO)}

The previous model assumes that peers do not spontaneously disconnect. TODO:
\begin{itemize}
\item assign nonzero disconnection rate to peers
\item show that this breaks the a.s. convergence to $\mbf S$
\item however we still have the property of mean-reversion
  but \tbf{below} $\mbf{S}$
\item one has to compensate for the disconnection rate by
  increasing the rate of adding new peers
\item in practice however this rate is unknown, one could imagine
  estimating it but that would probably be an attack vector
\item if one has a prior on the disconnection rate, we can
  fix $\mbf S$ to make it slightly higher than the target (show how to compute this)
\end{itemize}

\subsection{Building block 2: A probabilistic peer table}
\label{sec:probabilistic_peer_table}

A peer table is a data structure handling a finite sequence of all-distinct
peers $p_1 \cdots p_n \in \peers^\ast$. It is assumed throughout that this
invariant is maintained by the user of the table.

The data structure is parameterised by a
kick probability $\alpha \in ]0,1[$. We denote the empty  table by $\emptyset$.
If $T = p_1 \cdots p_n$ is a peer table, we denote $T\setminus p_i$ the table
corresponding to the subsequence with the $i$th element deleted.

A peer table is operated using the two following primitives:
\begin{itemize}
\item $\mathsf{insert} : \peers \times \peers^\ast \rar \peers^\ast$
\item $\mathsf{delete} : \peers^\ast \rar Pr(\peers^\ast)$
\end{itemize}
where $Pr(X)$ denotes the set of probability measures over $X$.
Note that deletion is the only probabilistic operation.
Insertion is defined trivially by prepending the peer at the beginning of the sequence:
\DAR{
  \mathsf{insert}(p, T) & = & p \cdot T
}
Deletion is defined recursively:
\begin{flalign}
  \label{def:deletion}
  \begin{array}{lll}
  \mathsf{delete}(p \cdot \emptyset) & = & \emptyset \\
  \mathsf{delete}(p_1 \cdot (p_2 \cdot T)) & = &
  \left\{ \begin{array}{ll} p_2 \cdot T & \text{with probability $\alpha$} \\
    p_1 \cdot \mathsf{delete}(p_2 \cdot T) & \text{with probability $1 - \alpha$}
  \end{array} \right.
  \end{array}
\end{flalign}
The following lemma make the basic property of this data
structure clear.
\begin{lemma}
  Let $T = p_1 \cdots p_n$ be a table with $n$ peers. Then
  for all $i < n$,
  $\prob(\mathsf{delete}(T) = T\setminus p_i) = (1 - \al)^{i - 1} \al$.
  Moreover, $\prob(\mathsf{delete}(T) = T\setminus p_n) = (1 - \al)^{n - 1}$.
\end{lemma}
In words, deletion follows a geometric distribution of parameter $\al$
truncated to $n$.
The net effect of this policy is that peers in the table are
less likely to be selected for deletion exponentially in their
depth. Since insertion proceeds by prepending, depth also correlates
with a notion of ``age'' of the peer. So older peer are probabilistically
protected from deletion.

\subsection{A maintenance algorithm}

The maintenance algorithm has the peer table at its core.
Following the assumptions outlined in Sec.~\ref{sec:modelling_assumptions},
it is enough to define the behaviour at wakeup times.
Recall that at each such time $t_k$, each peer defines
a set of potential new peers $\peers_p(t_k)$. In practice, it can happen that
all the new peers proposed are already in $\neighbours_{t_k}$ (a trivial but
unlikely instance where this happens corresponds to the case when
the connection graph is complete). However, this is mostly a distraction
from the core of the algorithm.

Thus, we decompose our presentation of the maintenance algorithm in two
steps: in the first step, we assume that arbitrarily many fresh peers
(all adversarial in the worst case) are available at each time step.
In the second step, we amend this algorithm to cope with situations
where a limited amount of fresh peers are available at each time step.

\subsubsection{Maintenance with unbounded fresh peers}

We suppose that at each wakeup time $t$, for each peer $p$,
the set $\peers_p(t)$ is nonempty and has empty intersection with $\neighbours_{t}$.
The algorithm makes use of the following constants:
\begin{itemize}
\item $\theta \in \R_{> 0}$: (non-normalised) wakeup rate
\item $\alpha \in ]0,1[$: parameter to the peer table
\item $total = 2 \times \mbf S \in \N$: maximum number of peers
\end{itemize}
The peer table is set to an arbitrary ordering of
the initial nonempty set of peers $\neighbours$.

%% TODO: Figure 1, maintenance in simulation mode with unbounded peers
\begin{center}
  \begin{algorithmic}
    \Procedure{Maintenance-loop}{}
    \State $T \gets \neighbours$
    \State $now \gets 0$
    \Loop
      \State $\Delta_t \gets sample(Exponential(\theta \times total))$
      \State $sleep(\Delta_t)$
      \State $now \gets now + \Delta_t$
      \State $kill \gets sample(Bernouilli(card(T)/total))$
      \If{kill}
      \State $T \gets \mathsf{delete}(T)$
      \Else
      \State $p \gets sample(Uniform(\cup_{p \in T} \peers_p(now)))$
      \State $T \gets \mathsf{insert}(p, T)$
      \EndIf
    \EndLoop
    \EndProcedure
  \end{algorithmic}
\end{center}

\subsubsection{Maintenance with bounded fresh peers (TODO)}

TODO...

\subsection{Analysing maintenance with unbounded fresh peers}

We want to compute the probability of an attack $(1-\eps)$-succeeding in $D$ units of time
or less, starting from an initial peer table of size $n$, with $total = 2 \times n$
and $adv(\neighbours_0) = 0$ (ie no adversarial peer in the table at time $t = 0$).
Let us denote this event $Eclipse(1-\eps, D)$.
We assume fixed a wakeup rate $\theta > 0$ and a kick probability $\alpha \in ]0,1[$.

Let $W_D$ be the random variable corresponding to the number of wakeup events in
$D$ time units. The sought probability is:
\begin{flalign}
  \label{eq:attack_success_probability}
  \prob(Eclipse(1-\eps, D)) = \sum_{i = 0}^\infty \prob(W_D = i) \cdot \prob(\text{attack $(1-\eps)$-succeeds in $i$ steps})
\end{flalign}
By definition of CTMCs, $W_D$ is Poisson distributed and $\prob(W_D = i)$ is easily obtained:
\[
\prob(W_D = i) = \frac{e^{-D\theta} (D \theta)^i}{i!}
\]
The interesting (and hard) part is to compute
$\prob(\text{attack $\eps$-succeeds in $i$ steps})$, ie the probability that an attack succeeds
in $i$ ``steps'' (or less), that is after $i$ calls to $\mathsf{delete}$ or $\mathsf{insert}$ -- assuming all peers inserted are adversarial.

Let us make this precise. To this end, we define the discrete-time ``peer table process'',
a Markov chain over peer tables. Assume given an infinite stream of adversarial
peers (the ordering is irrelevant).
Assuming the current state of the chain is the table $T$,
the jump probability is defined as follows:
\begin{itemize}
\item with probability $card(T)/total$, jump to table $T'$ with probability $delete(T)(T')$
  as computed in Eq.~\ref{def:deletion}.
\item with probability $1 - card(T)/total$, jump to table $\mathsf{insert}(a,T)$
  where $a$ is the next adversarial peer in the stream.
\end{itemize}

Let us say that a peer table is in well-formed if it can be written as:
\[
a_0 \cdot a_1 \cdots a_{n-1} \cdot p_0 \cdot p_1 \cdots p_{m-1}
\]
with the adversarial peers $a_i$ on top of the remaining original peers
$p_i$. Assuming all incoming peers are adversarial, one easily checks that
the operations of insertion and deletion preserve well-formedness.
A well-formed table $T$ is isomorphic to the pair $(m,n)$ of resp. non-adversarial
and adversarial peers contained in $T$. This mapping applied to the peer
table process defines the Markov chain $H$ defined below.

Let the probability of deleting any adversarial peer (in $a_{0 \ldots {n-1}}$) be
\DAR{
  k_a(0,0) & = & \bot \\
  k_a(0,n) & = & 1 \\
  k_a(m,n) & = & \sum_{i = 0}^{n-1} (1 - \al)^i \al
}
and let the probability of deleting any non-adversarial peer (in $p_{0 \ldots {m-1}}$) be
\DAR{
  k_p(0,0) & = & \bot \\
  k_p(m,0) & = & 1 \\
  k_p(m,n) & = & \left(\sum_{i = 0}^{m-2} (1 - \al)^{n + i} \al \right) + (1 - \al)^{n+m-1}
}
We define $H$ as:
\DAR{
  H((m,n),(m,n+1)) & = & 1 - (m+n)/total \\
  H((m,n),(m,n-1)) & = & (m+n)/total \times k_a(m,n) \\
  H((m,n),(m-1,n)) & = & (m+n)/total \times k_p(m,n) \\
}
The last case corresponds to the probability of deleting a non-adversarial peer.
One easily checks that $H$ is a left-stochastic matrix (columns sum to one).

$\prob(\text{attack $\eps$-succeeds in $i$ steps})$ can be obtained by computing
the $i$th power of $H$ starting from the initial state (eg with $n = 0$ and $m = total/2$)
and looking at the mass of the set of states where an attack $\eps$-succeeded.

Even though $H$ is very sparse, we were unable to find a closed form for $H^i$.
However we \emph{did} solve it numerically for fixed values $\al \in \ens{0.05, 0.1, 0.2}$ and $i \in \ens{1, \ldots, 10^4}$,
with initial state $(\frac{total}{2}, 0)$. Since we computed only up to a finite
power, the sum in Eq.~\ref{eq:attack_success_probability} cannot be computed exactly:
we have to truncate. Hence, the results presented below are approximated.

In order to simplify the time scaling, we pick a wakeup rate of $\theta = 1$.
We perform simulations of $10^4$ steps for values of $\alpha$ equal
to $\al = \ens{0.05, 0.1, 0.12, 0.15, 0.17, 0.2}$ (recall that the lower is $\al$,
the easier it is to kick peers deep in the table). For each $\al$,
we consider attack thresholds $1-\eps = 0.9$ and $1 - \eps = 0.5$.
We compute $\prob(Eclipse(1-\eps, D))$ over a time interval $D \in [1;5000]$.
Results are gathered in Fig.~\ref{fig:asp1} and Fig.~\ref{fig:asp2}.
Since we have to truncate the sum in Eq.~\ref{eq:attack_success_probability},
we also compute upper and lower bounds; however on the considered interval the
difference is negligible.

\begin{figure}
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.8\linewidth]{asp_alpha=0.05_theta=1.00_threshold=90.0.png}
    \label{fig:sfig1}
  \end{subfigure}
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.8\linewidth]{asp_alpha=0.10_theta=1.00_threshold=90.0.png}
    \label{fig:sfig2}
  \end{subfigure}
  \newline

  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.8\linewidth]{asp_alpha=0.12_theta=1.00_threshold=90.0.png}
    \label{fig:sfig3}
  \end{subfigure}
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.8\linewidth]{asp_alpha=0.15_theta=1.00_threshold=90.0.png}
    \label{fig:sfig4}
  \end{subfigure}
  \newline

  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.8\linewidth]{asp_alpha=0.17_theta=1.00_threshold=90.0.png}
    \label{fig:sfig5}
  \end{subfigure}
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.8\linewidth]{asp_alpha=0.20_theta=1.00_threshold=90.0.png}
    \label{fig:sfig6}
  \end{subfigure}

  \caption{Time-dependent attack success probabilities ($1-\eps = 0.9$)}
  \label{fig:asp1}
\end{figure}


\begin{figure}
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.8\linewidth]{asp_alpha=0.05_theta=1.00_threshold=50.0.png}
    \label{fig:sfig1}
  \end{subfigure}
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.8\linewidth]{asp_alpha=0.10_theta=1.00_threshold=50.0.png}
    \label{fig:sfig2}
  \end{subfigure}
  \newline

  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.8\linewidth]{asp_alpha=0.12_theta=1.00_threshold=50.0.png}
    \label{fig:sfig3}
  \end{subfigure}
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.8\linewidth]{asp_alpha=0.15_theta=1.00_threshold=50.0.png}
    \label{fig:sfig4}
  \end{subfigure}
  \newline

  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.8\linewidth]{asp_alpha=0.17_theta=1.00_threshold=50.0.png}
    \label{fig:sfig5}
  \end{subfigure}
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.8\linewidth]{asp_alpha=0.20_theta=1.00_threshold=50.0.png}
    \label{fig:sfig6}
  \end{subfigure}

  \caption{Time-dependent attack success probabilities ($1-\eps = 0.5$)}
  \label{fig:asp2}
\end{figure}

Let us compare the behaviour of the algorithm proposed here with the
naive (currently implemented) algorithm that removes peer uniformly at random.

\begin{figure}
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.8\linewidth]{asp_alpha=0.20_theta=1.00_threshold=90.0.png}
    \caption{New algorithm}
    \label{fig:sfig7}
  \end{subfigure}
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.8\linewidth]{asp_alpha=0.20_theta=1.00_threshold=90.0-legacy.png}
    \caption{Legacy}
    \label{fig:sfig8}
  \end{subfigure}
  \newline

  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.8\linewidth]{asp_alpha=0.20_theta=1.00_threshold=50.0.png}
    \caption{New algorithm}
    \label{fig:sfig9}
  \end{subfigure}
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.8\linewidth]{asp_alpha=0.20_theta=1.00_threshold=50.0-legacy.png}
    \caption{Legacy}
    \label{fig:sfig10}
  \end{subfigure}

  \caption{Comparing with uniform peer deletion}
  \label{fig:asp1}
\end{figure}

\subsection{Analysing maintenance with bounded fresh peers (TODO)}

TODO...

%% \newcommand{\delsym}{\mbf d}
%% \newcommand{\inssym}{\mbf i}

%% In order to simplify the analysis, we define a quotient of the peer table that we will show is bisimilar to the peer table process.

%% The sequence of $i$ calls is itself random. Let us adopt some simplying notations
%% in the following. A call to $\mathsf{delete}$ will be denoted $\delsym$ and a
%% call to $\mathsf{insert}$ will be denoted $\inssym$. Sequencing will be denoted
%% by string composition: for instance, a deltion followed by two insertions will correspond to
%% the sequence $\delsym \inssym \inssym$.

%% We consider random sequences of length $i$ in the alphabet $\ens{\delsym, \inssym}$.
%% These sequences are assigned a probability according to the discrete-time Ehrenfest
%% urn model (obtained by normalising the rate matrix $Q$). Concretely,
%% we define a family of measures $m_j \in \ens{\delsym, \inssym}^j$ for $j \in \ens{0;\ldots;i}$ inductively:
%% \DAR{
%%   m_0 & = & \eps ~ \text{with probability $1$} \\
%%   m_{i + 1} & = & \left\{ \begin{array}{l} \delsym \cdot m_i \\ m_i \cdot \inssym \end{array} \right.
%% }
%%  We start by
%% studying the structure of random words of length $N$ with
%% symbols $\delsym$ and $\inssym$ as generated by the following Markov chain,
%% an abstraction of the Maintenance loop:

%% \appendix

%% \section{Appendix (failed attempts)}

%% We propose an algorithm for maintenance inspired by the Moran
%% model of population genetics. Let us denote $s \in \N$ the
%% ``selective pressure'' (a parameter of the algorithm).
%% Consider a sequence of neighbourhoods $\neighbours_0, \ldots, \neighbours_k$.
%% The neighbourhood $\neighbours_{k+1}$ is computed as follows.

%% \begin{center}
%% \begin{algorithmic}
%%   \State $\mu \gets p \in \neighbours_k \mapsto \frac{age(p)}{\sum_{p \in \neighbours_k } age(p)}$
%%   \State $new \gets \emptyset$

%%   \For{$i \in \ens{1; \ldots; s}$}

%%   \State $p \gets sample(\mu)$

%%   \If {$P_k(p) \setminus \neighbours_k \neq \emptyset$}

%%   \State $p' \gets sample\_uniform(P_k(p) \setminus(\neighbours_k))$

%%   \State $new \gets new \cup \ens{sample(\mu)}$

%%   \EndIf

%%   \EndFor

%%   \State $preserved\_count \gets card(\neighbours_k) - card(new)$

%%   \State $preserved \gets \emptyset$

%%   \For{$i \in \ens{1; \ldots; preserved\_count}$}

%%   \State $p \gets sample(\mu)$

%%   \State $preserved \gets preserved \cup \ens{p}$

%%   \EndFor

%%   \State $\neighbours_{k+1} \gets preserved \cup $
%% \end{algorithmic}
%% \end{center}



%% \section{Inspiration}

%% \Itz{
%% \item Ehrenfest urn scheme to sample number of peers to add/delete, exchangeability,
%%   Moran process
%% \item Use a bandit to select who to sample the new peers from, reward = increase in 'quality' of neighbourhood graph
%% \item Intuitively, we want the neighbourhood graph to not be disconnected:
%%   if a peer proposes us a neighbourhood which is disconnected from the one we now, we should 'punish' him. We should not
%%   recursively punish the peer that proposed that peer, etc. because that could be
%%   used to slash innocent peers.
%% \item An attack we can't prevent but that must take \tbf{time}: we get well-connected peers and
%%   they simultaneously drop connections to the good subnetwork. So it is \tbf{not enough} to
%%   trust peers that propose well-connected new peers: we must preserve our old friends.
%%   It should take $O(n)$ rounds to kick a guy of age $n$: the longer we know somebody,
%%   the less likely to kick him. Eg instead of sampling $k$ peers to be kicked, sample
%%   $N-k$ peers to preserve from a distribution proportional to lifetime of a peer.
%% \item Can we do anything \emph{bayesian}?
%% }

%% \section{Selecting how many peers to remove or add}

%% In this section, we'll look at the specific problem of selecting the number of
%% peers to remove and the number of peers to add at each step.

%% \section{Ramblings}

%% Let's consider a Markov chain with state space $\Sigma = \N_{> 0}^k$.
%% Each component of this product corresponds to a counter.
%% The transition graph is constrained to respect the following constraint:
%% if $(a_1, \ldots, a_k)$ is the current state, the next state
%% must verify either $a'_i = a_i + 1$ or $a'_i = 1$, ie we can either
%% reset a counter to $1$ or increment it.

%% There's an interesting lumping of any Markov chain respecting this
%% transition graph: we can consider the random number of resets occuring
%% at any given state.

%% Let's look at the existence of
%% stationary distributions and equilibria of a bunch of Markov chains
%% respecting this constraint.

%% \subsection{Trial}

%% Consider the function over $\N_{> 0}$ defined by $h(n) = \frac{1}{n}$.
%% Consider the Markov chain $f : \Sigma \rar \prob(\Sigma)$ defined by
%% \DAR{
%%   f(a_{0}, \ldots, a_{n-1}) & = & (a_{0} + 1, \ldots, a_{j-1} + 1, 1, a_{j+1} + 1, \ldots, a_{k-1} + 1)
%% }
%% with probability proportional to $h(a_j)$.

%% To be precise, we reset only and exactly counter $j$ with probability
%% \DAR{
%%   P_{reset}(j) & = & \frac{1}{a_j} \frac{1}{\sum_{i =0}^{k-1} \frac{1}{a_i}} \\
%% }

%% \tbf{Q:} what is a probability over states that leaves this MC invariant?
%% \tbf{Q:} how to express neatly that the distribution of counters is ``exponential''?
%% Assume that $a_i = b^i$. Then:
%% \DAR{
%%   P_{reset}(j) & = & b^{-j} \frac{1}{\sum_{i =0}^{k-1} b^{-i}}
%%   & \approx & b^{-j} \frac{b-1}{b} \text{for large $k$}
%% }

%% \section{Finitely-branching CTMCs}

%% A CTMC is a particular kind of stochastic process.
%% Continuous-time stochastic can be described in several ways and equivalence
%% between these different presentations is not necessarily straightforward:
%% \begin{itemize}
%% \item families of random variables $\sequ{X_t}_{t}$
%%   (the law of $X_t$ corresponding to the distribution
%%   over the state space at time $t$)
%% \item time-dependent jump probabilities $P_t(x,y) = \mathbb P(X_t = x | X_0 = y)$
%%   (probability of jumping from $x$ to $y$ in $t$ units
%%   of time)
%% \item description by infinitesimal generators
%% \item operator semigroups
%% \item Stochastic differential equations, Partial differential
%%   equations, \ldots
%% \end{itemize}
%% We restrict our attention to the relatively simple case
%% of finitely-branching continuous-time
%% Markov-chains (CTMCs) on a countable state space $S$.

%% \subsection{The transition rate matrix}

%% A CTMC is typically given through a \emph{transition rate matrix}
%% $Q : S^2 \rar \R$ describing for each pair of
%% state $x, y$ the \emph{rate} at which the process
%% jumps from $x$ to $y$. The rate matrix verifies the
%% two following properties:
%% \begin{itemize}
%% \item $\forall x \neq y, Q_{xy} \ge 0$;
%% \item $\forall x, Q_{xx} = \sum_{y \neq x} Q_{xy}$
%%   (rows sum up to 0).
%% \end{itemize}

\small
\bibliography{notes}
\bibliographystyle{plain}

\end{document}
